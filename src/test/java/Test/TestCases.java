package Test;



import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Helper.helper;




public class TestCases {
	WebDriver driver;
	@BeforeTest
	void setBrowser()
	{

		driver=helper.startBrowser();
	}
	@Test
	void set()
	{
		try
		{
			helper.dashboard(driver);
			Thread.sleep(2000);
			helper.flight(driver);
		}
		catch(Exception e )
		{
			System.out.println(e);
		}

	}
	@AfterTest
	void close()
	{
		driver.close();
	}


}
