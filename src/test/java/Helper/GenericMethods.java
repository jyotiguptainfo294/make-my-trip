package Helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


class details
{
	public int maxPrice;
	public int minPrice;
	public int number;
}

public class GenericMethods {
	public static int  CurrencyToInt(String s)
	{

		int num=0;
		for(int i=0;i<s.length();i++)
		{
			if(s.charAt(i)!=',')
			{
				num=num*10+s.charAt(i)-'0';
			}


		}
		return num;
	}
	public static void GenerateMap(List<WebElement>flights,List<WebElement>prices,WebDriver driver)
	{
		HashMap<String, details> hmap = new HashMap<String, details>();


		int size=flights.size();
		System.out.println("Total Number of Flights "+size);
		for(int i=0;i<size;++i)
		{
			String c=flights.get(i).getText();
			String p=prices.get(i).getText();
			String[] paisa = p.split(" ");
			int rupees=GenericMethods.CurrencyToInt(paisa[1]);
			if(hmap.containsKey(c))
			{
				details present=hmap.get(c);
				present.number++;
				present.maxPrice=Math.max(present.maxPrice,rupees);
				present.minPrice=Math.min(present.minPrice,rupees);
				hmap.put(c,present);

			}
			else
			{
				details dt=new details();
				dt.number=1;
				dt.maxPrice=rupees;
				dt.minPrice=rupees;
				hmap.put(c, dt);
			}
		}
	
		System.out.println("Flight"+"  "+"Number"+"  "+"MinimumPrice"+"  "+"MaximumPrice");
		Iterator hmIterator = hmap.entrySet().iterator();
		while (hmIterator.hasNext()) { 
			Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
			String flight=(String) mapElement.getKey();
			details dt=(details) mapElement.getValue();
			int maxPrice=dt.maxPrice;
			int minPrice=dt.minPrice;
			int number=dt.number;
			System.out.println(flight+"   "+number+"   "+"   "+minPrice+"   "+maxPrice);

		}
	}

}
