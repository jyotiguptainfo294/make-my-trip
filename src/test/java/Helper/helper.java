package Helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import PageObject.DashBoard;
import PageObject.Flight;
import config.ConfigFileReader;
import io.github.bonigarcia.wdm.WebDriverManager;

public class helper {
	public  static WebDriver  startBrowser()
	{
		WebDriver driver;
		String url=ConfigFileReader.getConfigValue("url");
		String browserType=ConfigFileReader.getConfigValue("browser");
		if(browserType.equals("chrome"))
		{
			WebDriverManager.chromedriver().setup();	
			driver=new ChromeDriver();


		}
		else
		{
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();

		}
		driver.get(url);
		return driver;
	}


	public static void dashboard(WebDriver driver)
	{
		try
		{
			DashBoard db=new DashBoard(driver);
            String from="Delhi";
			db.selectFromcity(from);
           String to="Pune";
			db.setCity(to);
			String date="Wed Jan 29 2020";
			db.setDepartureDate(date);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void flight(WebDriver driver)
	{
		Flight f=new Flight(driver);
		f.scrollBottom();
		f.getFlight();

	}

}
