package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DashBoard {
	WebDriver driver;
	public DashBoard(WebDriver driver)
	{
		this.driver=driver;
	}
	public void selectFromcity(String from) throws InterruptedException
	{
		WebElement fromCity=driver.findElement(By.xpath(".//label[@for='fromCity']"));
		fromCity.click();
		WebElement delhi=driver.findElement(By.xpath(".//input[@placeholder=\"From\"]"));
		delhi.click();
		delhi.sendKeys("delhi");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//ul[@role='listbox']/li/div/div/p[contains(.,'"+from+"')]")).click();
	}
	public void selectTocity()
	{
		WebElement toCity=driver.findElement(By.xpath(".//label[@for=\"toCity\"]"));
		toCity.click();

	}
	public  void setCity(String to) throws InterruptedException
	{
		WebElement pune=driver.findElement(By.xpath(".//input[@placeholder=\"To\"]"));
		pune.click();
		pune.sendKeys("Pune , India");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//ul[@role='listbox']/li/div/div/p[contains(.,'"+to+"')]")).click();

	}
	public void setDepartureDate(String date)
	{
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement dat=wait.until(ExpectedConditions.elementToBeClickable( By.xpath(".//span[@class=\"selectedDateField appendBottom8\"]")));
		dat.click();

		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@aria-label='"+date+"']")));
		element.click();

		driver.findElement(By.xpath(".//a[@class=\"primaryBtn font24 latoBlack widgetSearchBtn \"]")).click();
	}
}
