package PageObject;


import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;

import Helper.GenericMethods;



public class Flight {
	WebDriver driver;
	public Flight(WebDriver driver)
	{
		this.driver=driver;
		WebElement element=driver.findElement(By.xpath(".//input[@id=\"toCity\"]"));
		Assert.assertTrue(element.isDisplayed());
	}
	
	public void scrollBottom()
	{
		try
		{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			long lastHeight =  (Long) js.executeScript("return document.body.scrollHeight");

			while (true) {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
				Thread.sleep(1000);

				long newHeight = (Long)js.executeScript("return document.body.scrollHeight");
				if (newHeight == lastHeight) {
					break;
				}
				lastHeight = newHeight;
			}
		}
		catch(Exception e)
		{
			System.out.println(e);

		}
	}
	public 	void getFlight()
	{	
		List<WebElement> flights=driver.findElements(By.xpath(".//span[@class='airways-name ']"));
		List<WebElement> prices=driver.findElements(By.xpath(".//span[@class='actual-price']"));
		GenericMethods.GenerateMap(flights,prices,driver);

	}



}

